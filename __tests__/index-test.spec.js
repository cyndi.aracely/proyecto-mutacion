const MutationBuilder = require('../src/builder/MutationBuilder');
describe('Validaciones de los ADNs mutación', () => {
    test('Validacion de mutacion 1 - con mutacion', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        const result = mutationValidator.setDna(['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG']).hasMutation();
        expect(result).toBe(true);
    });
    test('Validacion de mutacion 2 - sin mutacion', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        const result = mutationValidator.setDna(['ATGCGA', 'CAGTGC', 'TTATTT', 'AGTCGG', 'GCGTCA', 'TCACTG']).hasMutation();
        expect(result).toBe(false);
    });
    test('Validacion de mutacion 3 - con mutacion', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        const result = mutationValidator.setDna(['AAAAAA', 'CAGTGC', 'TTTTTT', 'AGACGG', 'CCCCCA', 'TGGGGG']).hasMutation();
        expect(result).toBe(true);
    });
    test('Validacion de mutacion 4 - con mutacion', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        const result = mutationValidator.setDna(['ATGCGA', 'ACGTGA', 'ATATTA', 'AGACGA', 'GCGTCC', 'TCACTG']).hasMutation();
        expect(result).toBe(true);
    });
    test('Validacion de mutacion 5 - con mutacion', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        const result = mutationValidator.setDna(['AGGCGA', 'CAGTTC', 'CTAGTT', 'CAAATG', 'CCTTCA', 'TCATTG']).hasMutation();
        expect(result).toBe(true);
    });
    test('Validacion de mutacion 6 - Con secuencia no permitida', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        expect(() => {
            mutationValidator.setDna(['ATGCGA', 'CAFTGC', 'TTLHTT', 'AGAXGG', 'GCGTCA', 'TCACTG']).hasMutation();
        }).toThrow()
    });
    test('Validacion de mutacion 7 - Con secuencia vacia', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        expect(() => {
            mutationValidator.setDna([]).hasMutation();
        }).toThrow()
    });
    test('Validacion de mutacion 8 - sin mutacion', () => {
        const mutationValidator = new MutationBuilder().validator().build();
        const result = mutationValidator.setDna(['AGGCA', 'TTCGG', 'CTAGC', 'AGCCC', 'CAGTC']).hasMutation();
        expect(result).toBe(false);
    });
});