const mongoose = require('mongoose');
const Mutation = require('../model/Mutation');

const Schema = mongoose.Schema;
// Esquema para la colección Mutación
const MutationSchema = new Schema({
    isMutated: {
        required: true,
        type: Boolean
    },
    dna: {
        required: true,
        unique: true,
        type: [String]
    }
},
    {
        timestamps: true,
    }
);
// Se carga el esquema en clase
MutationSchema.loadClass(Mutation);
const MutationModel = mongoose.model('Mutation', MutationSchema);
module.exports = MutationModel;