"use strict";
const { MongoClient } = require('mongodb');
const DBCredential = require('./DBCredential');
/**
 * Clase para realizar la conexion a la base de datos
 * @author Cynthia Chan
 */
class Database {
    static instance;
    database;
    dbClient;

    constructor() { };

    /**
     * Función encargada de crear (si es necesario) o retornar
     * la instancia de la conexión a la base de datos.
     * @returns 
     */
    static async getInstance() {
        return new Promise((resolve, reject) => {
            if (this.instance) {
                resolve(this.instance);
            }
            this.instance = new Database();
            // Conexión a la base de datos mongoDB
            this.instance.dbClient = new MongoClient(
                `mongodb+srv://${DBCredential.user}:${DBCredential.password}@${DBCredential.host}.mongodb.net/${DBCredential.dbName}?retryWrites=true&w=majority`, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            this.instance.client.connect((error) => {
                if (error) {
                    reject(error);
                }
                this.instance.database = this.instance.client.db(DBCredential.dbName);
                resolve(this.instance);
            });
        });
    }

    get db() {
        return Database.instance.database;
    }

    get client() {
        return Database.instance.dbClient;
    }
}
module.exports = Database;