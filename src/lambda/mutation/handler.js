const ResponseBuilder = require('../../builder/ResponseBuilder');
const Manager = require('./manager');
/**
 * Manejado de la lambda para validar la mutación genética
 * @param {*} event 
 * @param {*} context 
 * @param {*} callback 
 * @returns {ResponseBuilder}
 */
module.exports.hasMutationAction = async (event, context, callback) => {
    const responseModel = new ResponseBuilder();
    const mutationManager = new Manager();
    try {
        const { body } = event;
        const { dna } = JSON.parse(body);
        // Validación
        const result = await mutationManager.hasMutation(dna);
        // Respuestas
        if (!result || typeof result === undefined) {
            return responseModel.handleError(result == false ? 'ADN sin mutación' : 'Solicitud incorrecta');
        }
        return responseModel.handlerReponse({ resultado: result, mensaje: 'ADN con mutación' });
    } catch (error) {
        return responseModel.handleError(error.message ?? '');
    }
}

/**
 * Manejado de la lambda para estadísticas de la mutación genética
 * @param {*} event 
 * @param {*} context 
 * @param {*} callback 
 * @returns {ResponseBuilder}
 */
 module.exports.statsAction = async (event, context, callback) => {
    const responseModel = new ResponseBuilder();
    const mutationManager = new Manager();
    try {
        // consulta estadística
        const result = await mutationManager.stats();
        // Respuestas
        if (!result || typeof result === undefined) {
            return responseModel.handleError('Sin resultados');
        }
        return responseModel.handlerReponse({ resultado: result, mensaje: 'Estadísticas' });
    } catch (error) {
        return responseModel.handleError(error.message ?? '');
    }
}