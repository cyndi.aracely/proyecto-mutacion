const MutationBuilder = require('../../builder/MutationBuilder');
const DatabaseBuilder = require('../../builder/DatabaseBuilder');
const DatabaseQuery = require('../../builder/DatabaseQuery');
/**
 * Clase que contienen la lógica de negocio - Mutación
 */
class Manager {
    // Validador si el SDN recibido tiene mutación
    hasMutation = async (dna) => {
        let isMutated;
        const mutationValidator = new MutationBuilder().validator().build();
        isMutated = mutationValidator.setDna(dna).hasMutation();
        // Guardar en la base de datos
        const modelMutation = {
            isMutated: mutationValidator.mutation.isMutated,
            dna: mutationValidator.mutation.dna
        };
        new DatabaseBuilder(modelMutation).insert().execute();
        return isMutated;
    }

    // Validador si el SDN recibido tiene mutación
    stats = async () => {
        const modelMutation = {
            isMutated: true
        };
        const modelNoMutation = {
            isMutated: false
        };
        const query = new DatabaseQuery('', modelMutation);
        await query.countSQL();
        const mutationStats = new MutationBuilder().stats()
            .build()
            .setCountMutations(query.model.countMutations)
            .setCountNoMutations(query.model.countNoMutations);
        const data = {
            count_mutations: mutationStats.mutation.countMutations,
            count_no_mutation: mutationStats.mutation.countNoMutations,
            ratio: mutationStats.getRation()
        }
        return data;
    }
}
module.exports = Manager;