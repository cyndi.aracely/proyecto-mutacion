class MutationStats {
    constructor(mutation) {
        this.mutation = mutation;
    }

    setCountMutations = (countMutations) => {
        this.mutation.countMutations = countMutations;
        return this;
    }

    setCountNoMutations = (countNoMutations) => {
        this.mutation.countNoMutations = countNoMutations;
        return this;
    }

    getRation() {
       return this.mutation.countNoMutations ? ( this.mutation.countMutations / this.mutation.countNoMutations) : 0;
    }
}
module.exports = MutationStats;
