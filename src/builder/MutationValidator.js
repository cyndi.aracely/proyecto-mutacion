/**
 * Clase para la validación del ADN
 */
class MutationValidator {
    dnaValidationRegex = /^[ACGT]+$/;
    mutacionesAparecidad = 0;
    constructor(mutation) {
        this.mutation = mutation;
    }

    setDna(dna) {
        this.mutation.dna = Object.values(dna);
        return this;
    }

    setIsMutated(isMutated) {
        this.mutation.isMutated = isMutated;
        return this;
    }
    // Validacion de mutacion
    hasMutation() {
        if (this.mutation.dna.length == 0) {
            throw new Error('El ADN inválido, no pude ser vacio');
        }
        if (this.validateStructure()) {
            throw new Error('El ADN inválido, solo puede contener A, T, C y G');
        }
        this.valitador();
        return this.mutation.isMutated;
    }
    // inicializador para el validador
    valitador() {
        this.validateVertical();
        if (this.mutacionesAparecidad < 2) {
            this.validateHorizontal();
        }
        if (this.mutacionesAparecidad < 2) {
            this.validateDiagonal();
        }
        if (this.mutacionesAparecidad < 2) {
            this.validateDiagonalInvert();
        }
        if (this.mutacionesAparecidad >= 2) {
            this.mutation.isMutated = true;
        } else {
            this.mutation.isMutated = false;
        }
    }
    // Valida la estructura con una expresión regular
    validateStructure() {
        return this.mutation.dna.some((item) => {
            return !this.dnaValidationRegex.test(item.toUpperCase());
        });
    }
    // Valida combinaciones que se consideran mutantes
    validateCombinacionesMutantes(adnSecuencia) {
        const combinacionesMutantes = ['AAAA', 'CCCC', 'GGGG', 'TTTT'];
        combinacionesMutantes.some((secuencia) => {
            if (adnSecuencia.includes(secuencia)) {
                this.mutacionesAparecidad++;
            }
        });
    }
    // Valida las lineas horizontales
    validateHorizontal() {
        this.mutation.dna.some((item) => {
            this.validateCombinacionesMutantes(item.toUpperCase());
        });
    }
    // Valida las lineas verticales
    validateVertical() {
        const adnInvertida = [];
        for (let x = 0; x < this.mutation.dna.length; x++) {
            let adnSecuencia = '';
            for (let y = 0; y < this.mutation.dna.length; y++) {
                adnSecuencia = adnSecuencia + this.mutation.dna[y][x];
            }
            adnInvertida.push(adnSecuencia);
        }
        adnInvertida.some((item) => {
            this.validateCombinacionesMutantes(item.toUpperCase());
        });
    }
    // Valida las diagonales de arriba hacia abajo y viseversa
    validateDiagonal() {
        const adnDiagonalparaAbajo = [];
        const adnDiagonalparaArriba = [];
        // Diagonal para abajo
        let baseX = 1;
        while (this.mutation.dna.length - baseX >= 4) {
            let adnSecuencia = '';
            for (let x = baseX; x < this.mutation.dna.length; x++) {
                adnSecuencia = adnSecuencia + this.mutation.dna[x - baseX][x];
            }
            adnDiagonalparaAbajo.push(adnSecuencia);
            baseX++;
        }
        adnDiagonalparaAbajo.some((item) => {
            this.validateCombinacionesMutantes(item.toUpperCase());
        });
        // Diagonal para arriba
        let baseY = 0;
        while (this.mutation.dna.length - baseY >= 4) {
            let adnSecuencia = '';
            for (let y = baseY; y < this.mutation.dna.length; y++) {
                adnSecuencia = adnSecuencia + this.mutation.dna[y][y - baseY];
            }
            adnDiagonalparaArriba.push(adnSecuencia);
            baseY++;
        }
        adnDiagonalparaArriba.some((item) => {
            this.validateCombinacionesMutantes(item.toUpperCase());
        });
    }
    // Valida las diagonales invertidas de arriba hacia abajo y viseversa
    validateDiagonalInvert() {
        const adnDiagonalparaAbajo = [];
        const adnDiagonalparaArriba = [];
        // Diagonal para abajo
        let baseXInvertida = 1
        while (this.mutation.dna.length - baseXInvertida >= 4) {
            let baseX = this.mutation.dna.length - (1) - baseXInvertida;
            let adnSecuencia = '';
            for (let x = baseX; x >= 0; x--) {
                adnSecuencia = adnSecuencia + this.mutation.dna[baseX - x][x];
            }
            adnDiagonalparaAbajo.push(adnSecuencia);
            baseXInvertida++;
        }
        adnDiagonalparaAbajo.some((item) => {
            this.validateCombinacionesMutantes(item.toUpperCase());
        });
        // Diagonal para arriba
        let baseY = 0;
        let invertir = this.mutation.dna - (1);
        while (this.mutation.dna.length - baseY >= 4) {
            let adnSecuencia = '';
            for (let y = baseY; y < this.mutation.dna.length; y++) {
                adnSecuencia = adnSecuencia + this.mutation.dna[y][invertir - y - baseY];
            }
            adnDiagonalparaArriba.push(adnSecuencia);
            baseY++;
        }
        adnDiagonalparaArriba.some((item) => {
            this.validateCombinacionesMutantes(item.toUpperCase());
        });
    }
}
module.exports = MutationValidator;