const Mutation = require('../model/Mutation');
const MutationValidator = require('./MutationValidator');
const MutationStats = require('./MutationStats');
/**
 * Clases para crear Objetos de acuerdo a su función
 */
class MutationBuilder {
    constructor() {
        this.mutation = new Mutation();
    }
    /** Objeto para validar */
    validator = () => {
        this.mutation = new MutationValidator(this.mutation);
        return this;
    }
    /** Objeto para estadísticas */
    stats = () => {
        this.mutation = new MutationStats(this.mutation);
        return this;
    }
    /** Retorna el objeto creado */
    build = () => {
        return this.mutation;
    }
}
module.exports = MutationBuilder