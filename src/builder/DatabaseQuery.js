const Database = require('../config/Database');
class DatabaseQuery {
    model = null;
    constructor(execute, model) {
        this.model = model;
        switch (execute) {
            case 'insert':
                this.insert();
                break;
            case 'all':
                this.countSQL();
                break;
            default:
                break;
        }
    }

    insert() { // inserta en la bd
        Database.getInstance().then((dbi) => {
            dbi.db.collection('mutacion').updateOne({ dna: this.model.dna }, { $set: this.model }, { upsert: true }).then(mutation => {
                this.model = mutation
                dbi.client.close();
            });
        }).catch((error) => console.error(error));
    }

    countSQL = async () => { // Consutar registros
        const dbi = await Database.getInstance();
        this.model.countMutations = await dbi.db.collection('mutacion').countDocuments({ isMutated: true });
        this.model.countNoMutations = await dbi.db.collection('mutacion').countDocuments({ isMutated: false });
    }
}
module.exports = DatabaseQuery;