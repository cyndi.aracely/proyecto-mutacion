const DatabaseQuery = require('./DatabaseQuery');
class DatabaseBuilder {
    model = null;
    query = '';
    constructor(model) {
        this.model = model;
    }
    insert = () => {
        this.query = "insert";
        return this;
    }

    all = () => {
        this.query = "all";
        return this;
    }

    execute = () => {
        return new DatabaseQuery(this.query, this.model)
    };
}
module.exports = DatabaseBuilder;