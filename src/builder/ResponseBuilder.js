/**
 * Clase que construye la estructura de la respuesta
 */
class ResponseBuilder {
    /**
     * Función que retorna los encabezados de la respuesta
     * @returns {headers}
     */
    setHeaders() {
        const headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Access-Control-Allow-Headers':
                'timestamp,tz,tenant-id,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        }
        return headers;
    }

    /**
    * Función que retorna la respuesta exitosa
    * @param {*} body
    * @return {CommonResponse}
    */
    handlerReponse = (body) => {
        let customResponse;
        if (body) {
            const bd = {
                message: '',
                status: 'success',
                error: false,
                data: body
            }
            customResponse = {
                statusCode: 200,
                body: JSON.stringify(bd),
                headers: this.setHeaders()
            }
        } else {
            customResponse = {
                statusCode: 200,
                body: '',
                headers: this.setHeaders()
            }
        }
        return customResponse
    }

    /**
    * Función que retorna la respuesta de error
    * @param {string} errorMsg
    * @return {CommonResponse}
    */
    handleError = (errorMsg) => {
        const bd = {
            message: errorMsg,
            status: 'forbidden',
            error: true,
            data: null
        }
        const customError = {
            statusCode: 403,
            body: JSON.stringify(bd),
            headers: this.setHeaders()
        }
        return customError
    }

}
module.exports = ResponseBuilder;