/**
 * Clase para el modelo Mutation (collection bd)
 */
class Mutation {
    constructor() {
        this.dna = [];
        this.isMutated = false;
        this.countMutations = 0;
        this.countNoMutation = 0;
        this.ratio = 0;
    }
}
module.exports = Mutation;