# Proyecto de Mutación Genética Serverless

Este proyecto detecta si una persona tiene diferencias genéticas basándose en su secuencia de ADN.

## Configuración por anticipado

Descarga de [nodejs](https://nodejs.org/es/)

Descarga de [serverless](https://www.serverless.com/).

```
npm install -g serverless
```

### Desarrollo local

Instalar plugins (dependencias) con el siguiente comando:

```
npm install
```

Plugin serverless-offline (requerido)

```bash
serverless plugin install -n serverless-offline
```

Para su inicar el proyecto de forma local ejecutar el siguiente comando:

```
serverless offline
```

### Uso de la api

Despues del despliegue del proyecto, se pude consultar la siguiente api's:

```bash
post http://localhost:3000/dev/mutation/
```